#Inciar pantalla principal
from tkinter import *
from tkinter import filedialog,messagebox
import random
import time

def reiniciar_2():
    textcaptura.delete(1.0, END)
    e_clasica.set('0')
    e_hawaiana.set('0')
    e_italiana.set('0')
    e_tocino.set('0')
    e_mexicana.set('0')
    h_clasica.set('0')
    h_hawaiana.set('0')
    h_italiana.set('0')
    h_tocino.set('0')
    h_mexicana.set('0')
    x_pulpos.set('0')
    x_papafran.set('0')
    x_salchipapa.set('0')
    x_momia.set('0')
    x_cebolla.set('0')
    x_dedosdq.set('0')
    x_nuggets.set('0')
    b_pepsi.set('0')
    b_manzana.set('0')
    b_mirinda.set('0')
    b_seven.set('0')
    b_sangria.set('0')
    b_delaware.set('0')
    b_jumex.set('0')
    b_refresco.set('0')
    b_bonafont.set('0')
    b_cocacola.set('0')
    b_refresco2.set('0')
    b_bonafontlevite.set('0')
    a_bbq.set('0')
    a_bbq12.set('0')
    a_bbq18.set('0')
    a_bufalo.set('0')
    a_bufalo12.set('0')
    a_bufalo18.set('0')
    a_mango.set('0')
    a_mango12.set('0')
    a_mango18.set('0')
    a_piña.set('0')
    a_piña12.set('0')
    a_piña18.set('0')
    a_habanero.set('0')
    a_habanero12.set('0')
    a_habanero18.set('0')
    a_tamarindo.set('0')
    a_tamarindo12.set('0')
    a_tamarindo18.set('0')
    a_quesopar.set('0')
    a_quesopar12.set('0')
    a_quesopar18.set('0')
    a_valentina.set('0')
    a_valentina12.set('0')
    a_valentina18.set('0')
    a_ranch.set('0')
    a_ranch12.set('0')
    a_ranch18.set('0')

    costo_comida.set('')
    costo_bebida.set('')
    costo_total.set('')

def guardar_2():
    url = filedialog.asksaveasfile(mode='w', defaultextension='.txt')

    orden = textcaptura.get(1.0, END)
    url.write(orden)
    url.close()
    messagebox.showinfo('Orden guardada con exito')
def capturarorden():
    textcaptura.delete(1.0, END)
    x = random.randint(100,10000)
    numerodeorden = 'N° Orden'+str(x)

    date = time.strftime('%d/%m/%Y')
    textcaptura.insert(END,'CAPTURAR ORDEN:\t\t'+numerodeorden+'\t\t'+date+'\t\t')
    textcaptura.insert(END,'------------------------------------------------------------------------------------')
    textcaptura.insert(END, 'COMPRADO:\t\t  COSTO: \t\t'+'\t\t')
    textcaptura.insert(END,'------------------------------------------------------------------------------------')
    if e_clasica.get()!='0':
        textcaptura.insert(END, f'Hamburguesa Clasica\t\t\t{int(e_clasica.get())*56}'+'\t\t\t')
    if e_hawaiana.get()!='0':
        textcaptura.insert(END, f'Hamburguesa Hawaiana\t\t\t{int(e_hawaiana.get())*65}'+'\t\t\t')
    if e_italiana.get()!='0':
        textcaptura.insert(END, f'Hamburguesa Italiana\t\t\t{int(e_italiana.get())*65}'+'\t\t\t')
    if e_tocino.get()!='0':
        textcaptura.insert(END, f'Hamburguesa BBQ Tocino\t\t\t{int(e_tocino.get())*70}'+'\t\t\t')
    if e_mexicana.get()!='0':
        textcaptura.insert(END, f'Hamburguesa Mexicana\t\t\t{int(e_mexicana.get())*70}'+'\t\t\t')
    if h_clasica.get()!='0':
        textcaptura.insert(END, f'Hot Dog Clasico\t\t\t{int(h_clasica.get())*22}'+'\t\t\t')
    if h_hawaiana.get()!='0':
        textcaptura.insert(END, f'Hot Dog  Hawaiano\t\t\t{int(h_hawaiana.get())*30}'+'\t\t\t')
    if h_italiana.get()!='0':
        textcaptura.insert(END, f'Hot Dog  Italiano\t\t\t{int(h_italiana.get())*30}'+'\t\t\t')
    if h_tocino.get()!='0':
        textcaptura.insert(END, f'Hot Dog  BBQ Tocino\t\t\t{int(h_tocino.get())*34}'+'\t\t\t')
    if h_mexicana.get()!='0':
        textcaptura.insert(END, f'Hot Dog  Mexicana\t\t\t{int(h_mexicana.get())*34}'+'\t\t\t')
    if x_pulpos.get()!='0':
        textcaptura.insert(END, f'Salchipulpos\t\t\t{int(x_pulpos.get())*30}'+'\t\t\t')
    if x_papafran.get()!='0':
        textcaptura.insert(END, f'Papas a la Francesa\t\t\t{int(x_papafran.get())*50}'+'\t\t\t')
    if x_salchipapa.get()!='0':
        textcaptura.insert(END, f'Salchipapas\t\t\t{int(x_salchipapa.get())*52}'+'\t\t\t')
    if x_momia.get()!='0':
        textcaptura.insert(END, f'Salchimomias\t\t\t{int(x_momia.get())*50}'+'\t\t\t')
    if x_cebolla.get()!='0':
        textcaptura.insert(END, f'Aros de cebolla\t\t\t{int(x_cebolla.get())*52}'+'\t\t\t')
    if x_dedosdq.get()!='0':
        textcaptura.insert(END, f'Dedos de Queso\t\t\t{int(x_dedosdq.get())*60}'+'\t\t\t')
    if x_nuggets.get()!='0':
        textcaptura.insert(END, f'Nuggets de Pollo\t\t\t{int(x_nuggets.get())*50}'+'\t\t\t')
    if b_pepsi.get()!='0':
        textcaptura.insert(END, f'Pepsi\t\t\t{int(b_pepsi.get())*17}'+'\t\t\t')
    if b_manzana.get()!='0':
        textcaptura.insert(END, f'Manzanita\t\t\t{int(b_manzana.get())*17}'+'\t\t\t')
    if b_mirinda.get()!='0':
        textcaptura.insert(END, f'Mirinda\t\t\t{int(b_mirinda.get())*17}'+'\t\t\t')
    if b_seven.get()!='0':
        textcaptura.insert(END, f'7Up\t\t\t{int(b_seven.get())*17}'+'\t\t\t')
    if b_sangria.get()!='0':
        textcaptura.insert(END, f'Sangria\t\t\t{int(b_sangria.get())*17}'+'\t\t\t')
    if b_delaware.get()!='0':
        textcaptura.insert(END, f'Delaware Punch\t\t\t{int(b_delaware.get())*22}'+'\t\t\t')
    if b_jumex.get()!='0':
        textcaptura.insert(END, f'Jumex\t\t\t{int(b_jumex.get())*22}'+'\t\t\t')
    if h_italiana.get()!='0':
        textcaptura.insert(END, f'Refresco 600ml\t\t\t{int(b_refresco.get())*22}'+'\t\t\t')
    if b_cocacola.get()!='0':
        textcaptura.insert(END, f'Coca Cola\t\t\t{int(b_cocacola.get())*25}'+'\t\t\t')
    if b_refresco2.get()!='0':
        textcaptura.insert(END, f'Refresco 2l\t\t\t{int(b_refresco2.get())*40}'+'\t\t\t')
    if a_bbq.get()!='0':
        textcaptura.insert(END, f'Alitas BBQ x6\t\t\t{int(a_bbq.get())*60}'+'\t\t\t')
    if a_bbq12.get()!='0':
        textcaptura.insert(END, f'Alitas BBQ x12\t\t\t{int(a_bbq12.get())*110}'+'\t\t\t')
    if a_bbq18.get()!='0':
        textcaptura.insert(END, f'Alitas BBQ x18\t\t\t{int(a_bbq18.get())*160}'+'\t\t\t')
    if a_bufalo.get()!='0':
        textcaptura.insert(END, f'Alitas Bufalo x6\t\t\t{int(a_bufalo.get())*60}'+'\t\t\t')
    if a_bufalo12.get()!='0':
        textcaptura.insert(END, f'Alitas Bufalo x12\t\t\t{int(a_bufalo12.get())*110}'+'\t\t\t')
    if a_bufalo18.get()!='0':
        textcaptura.insert(END, f'Alitas Bufalo x18\t\t\t{int(a_bufalo18.get())*160}'+'\t\t\t')
    if a_mango.get()!='0':
        textcaptura.insert(END, f'Alitas Mango x6\t\t\t{int(a_mango.get())*60}'+'\t\t\t')
    if a_mango12.get()!='0':
        textcaptura.insert(END, f'Alitas Mango x12\t\t\t{int(a_mango12.get())*110}'+'\t\t\t')
    if a_mango18.get()!='0':
        textcaptura.insert(END, f'Alitas Mango x18\t\t\t{int(a_mango18.get())*160}'+'\t\t\t')
    if a_piña.get()!='0':
        textcaptura.insert(END, f'Alitas Piña x6\t\t\t{int(a_piña.get())*60}'+'\t\t\t')
    if a_piña12.get()!='0':
        textcaptura.insert(END, f'Alitas Piña x12\t\t\t{int(a_piña12.get())*110}'+'\t\t\t')
    if a_piña18.get()!='0':
        textcaptura.insert(END, f'Alitas Piña x18\t\t\t{int(a_piña18.get())*160}'+'\t\t\t')
    if a_habanero.get()!='0':
        textcaptura.insert(END, f'Alitas Habanero x6\t\t\t{int(a_habanero.get())*60}'+'\t\t\t')
    if a_habanero12.get()!='0':
        textcaptura.insert(END, f'Alitas Habanero x12\t\t\t{int(a_habanero12.get())*110}'+'\t\t\t')
    if a_habanero18.get()!='0':
        textcaptura.insert(END, f'Alitas Habanero x18\t\t\t{int(a_habanero18.get())*160}'+'\t\t\t')
    if a_tamarindo.get()!='0':
        textcaptura.insert(END, f'Alitas Tamarindo x6\t\t\t{int(a_tamarindo.get())*60}'+'\t\t\t')
    if a_tamarindo12.get()!='0':
        textcaptura.insert(END, f'Alitas Tamarindo x12\t\t\t{int(a_tamarindo12.get())*110}'+'\t\t\t')
    if a_tamarindo18.get()!='0':
        textcaptura.insert(END, f'Alitas Tamarindo x18\t\t\t{int(a_tamarindo18.get())*160}'+'\t\t\t')
    if a_quesopar.get()!='0':
        textcaptura.insert(END, f'Alitas Queso Parmesano x6\t\t\t{int(a_quesopar.get())*60}'+'\t\t\t')
    if a_quesopar12.get()!='0':
        textcaptura.insert(END, f'Alitas Queso Parmesano x12\t\t\t{int(a_quesopar12.get())*110}'+'\t\t\t')
    if a_quesopar18.get()!='0':
        textcaptura.insert(END, f'Alitas Queso Parmesano x18\t\t\t{int(a_quesopar18.get())*160}'+'\t\t\t')
    if a_valentina.get()!='0':
        textcaptura.insert(END, f'Alitas Valentina x6\t\t\t{int(a_valentina.get())*60}'+'\t\t\t')
    if a_valentina12.get()!='0':
        textcaptura.insert(END, f'Alitas Valentina x12\t\t\t{int(a_valentina12.get())*110}'+'\t\t\t')
    if a_valentina18.get()!='0':
        textcaptura.insert(END, f'Alitas Valentina x18\t\t\t{int(a_valentina18.get())*160}'+'\t\t\t')
    if a_ranch.get()!='0':
        textcaptura.insert(END, f'Alitas Ranch x6\t\t\t{int(a_ranch.get())*60}'+'\t\t\t')
    if a_ranch12.get()!='0':
        textcaptura.insert(END, f'Alitas Ranch x12\t\t\t{int(a_ranch12.get())*110}'+'\t\t\t')
    if a_ranch18.get()!='0':
        textcaptura.insert(END, f'Alitas Ranch x18\t\t\t{int(a_ranch18.get())*160}'+'\t\t\t')


    textcaptura.insert(END,'------------------------------------------------------------------------------------')
    textcaptura.insert(END, f'Costo total:\t\t\t{preciototal}')

    #Funciones Hamburguesas
def total_costo():
    global preciototal

    item1 = int(e_clasica.get())
    item2 = int(e_hawaiana.get())
    item3 = int(e_italiana.get())
    item4 = int(e_tocino.get())
    item5 = int(e_mexicana.get())
    item6 = int(h_clasica.get())
    item7 = int(h_hawaiana.get())
    item8 = int(h_italiana.get())
    item9 = int(h_tocino.get())
    item10 = int(h_mexicana.get())
    item11 = int(x_pulpos.get())
    item12 = int(x_papafran.get())
    item13 = int(x_salchipapa.get())
    item14 = int(x_momia.get())
    item15 = int(x_cebolla.get())
    item16 = int(x_dedosdq.get())
    item17 = int(x_nuggets.get())
    item18 = int(b_pepsi.get())
    item19 = int(b_manzana.get())
    item20 = int(b_mirinda.get())
    item21 = int(b_seven.get())
    item22 = int(b_sangria.get())
    item23 = int(b_delaware.get())
    item24 = int(b_jumex.get())
    item25 = int(b_refresco.get())
    item26 = int(b_bonafont.get())
    item27 = int(b_cocacola.get())
    item28 = int(b_refresco2.get())
    item29 = int(b_bonafontlevite.get())
    item30 = int(a_bbq.get())
    item31 = int(a_bufalo.get())
    item32 = int(a_mango.get())
    item33 = int(a_piña.get())
    item34 = int(a_habanero.get())
    item35 = int(a_tamarindo.get())
    item36 = int(a_quesopar.get())
    item37 = int(a_valentina.get())
    item38 = int(a_ranch.get())
    item39 = int(a_bbq12.get()) #12
    item40 = int(a_bbq18.get())
    item41 = int(a_bufalo12.get())#12
    item42 = int(a_bufalo18.get())
    item43 = int(a_mango12.get())#12
    item44 = int(a_mango18.get())
    item45 = int(a_piña12.get())#12
    item46 = int(a_piña18.get())
    item47 = int(a_habanero12.get())#12
    item48 = int(a_habanero18.get())
    item49 = int(a_tamarindo12.get())#12
    item50 = int(a_tamarindo18.get())
    item51 = int(a_quesopar12.get())#12
    item52 = int(a_quesopar18.get())
    item53 = int(a_valentina12.get())#12
    item54 = int(a_valentina18.get())
    item55 = int(a_ranch12.get())#12
    item56 = int(a_ranch18.get())

    preciocomida = (item1*56)+(item2*65)+(item3*65)+(item4*70)+(item5*70)+(item6*22)+(item7*30)+(item8*30)+(item9*34)+(item10*34)+(item11*30)+(item12*50)+(item13*52)+(item14*50)+(item15*52)+(item16*60)+(item17*50)+(item30*60)+(item31*60)+(item32*60)+(item33*60)+(item34*60)+(item35*60)+(item36*60)+(item37*60)+(item38*60)+(item39*110)+(item40*160)+(item41*110)+(item42*160)+(item43*110)+(item44*160)+(item45*110)+(item46*160)+(item47*110)+(item48*160)+(item49*110)+(item50*160)+(item51*110)+(item52*160)+(item53*110)+(item54*160)+(item55*110)+(item56*160)
    preciobebida = (item18*17)+(item19*17)+(item20*17)+(item21*17)+(item22*17)+(item23*22)+(item24*22)+(item25*22)+(item27*25)+(item28*40)
    # +(item26*)+(item29*)
    preciototal = preciobebida + preciocomida

    costo_comida.set(str(preciocomida)+'.000$')
    costo_bebida.set(str(preciobebida)+'.000$')
    costo_total.set(str(preciototal)+'.000$')
def clas():
    if var1.get()==1:
        textclasica.config(state=NORMAL)
        textclasica.delete(0, END)
        textclasica.focus()
    else:
        textclasica.config(state=DISABLED)
        e_clasica.set('0')

def haw():
    if var2.get()==1:
        texthawaiana.config(state=NORMAL)
        texthawaiana.delete(0, END)
        texthawaiana.focus()
    else:
        texthawaiana.config(state=DISABLED)
        e_hawaiana.set('0')

def ita():
    if var3.get()==1:
        textitaliana.config(state=NORMAL)
        textitaliana.delete(0, END)
        textitaliana.focus()
    else:
        textitaliana.config(state=DISABLED)
        e_italiana.set('0')

def tocino():
    if var4.get()==1:
        texttocino.config(state=NORMAL)
        texttocino.delete(0, END)
        texttocino.focus()
    else:
        texttocino.config(state=DISABLED)
        e_tocino.set('0')
    
def mexi():
    if var5.get()==1:
        textmexicana.config(state=NORMAL)
        textmexicana.delete(0, END)
        textmexicana.focus()
    else:
        textmexicana.config(state=DISABLED)
        e_mexicana.set('0')

#Funciones Hotgdogs
def hot_clas():
    if var6.get()==1:
        textclasica_hot.config(state=NORMAL)
        textclasica_hot.delete(0, END)
        textclasica_hot.focus()
    else:
        textclasica_hot.config(state=DISABLED)
        h_clasica.set('0')

def hot_haw():
    if var7.get()==1:
        texthawaiana_hot.config(state=NORMAL)
        texthawaiana_hot.delete(0, END)
        texthawaiana_hot.focus()
    else:
        texthawaiana_hot.config(state=DISABLED)
        h_hawaiana.set('0')

def hot_ita():
    if var8.get()==1:
        textitaliana_hot.config(state=NORMAL)
        textitaliana_hot.delete(0, END)
        textitaliana_hot.focus()
    else:
        textitaliana_hot.config(state=DISABLED)
        h_italiana.set('0')

def hot_tocino():
    if var9.get()==1:
        texttocino_hot.config(state=NORMAL)
        texttocino_hot.delete(0, END)
        texttocino_hot.focus()
    else:
        texttocino_hot.config(state=DISABLED)
        h_tocino.set('0')

def hot_mex():
    if var10.get()==1:
        textmexicana_hot.config(state=NORMAL)
        textmexicana_hot.delete(0, END)
        textmexicana_hot.focus()
    else:
        textmexicana_hot.config(state=DISABLED)
        h_mexicana.set('0')

#Funciones Alitas
def bbq_a():
    if var30.get()==1:
        textbbq.config(state=NORMAL)
        textbbq.delete(0, END)
        textbbq.focus()
    else:
        textbbq.config(state=DISABLED)
        a_bbq.set('0')

def bbq_a12():
    if var39.get()==1:
        textbbq12.config(state=NORMAL)
        textbbq12.delete(0, END)
        textbbq12.focus()
    else:
        textbbq12.config(state=DISABLED)
        a_bbq12.set('0')

def bbq_a18():
    if var40.get()==1:
        textbbq18.config(state=NORMAL)
        textbbq18.delete(0, END)
        textbbq18.focus()
    else:
        textbbq18.config(state=DISABLED)
        a_bbq18.set('0')

def bufalo_a():
    if var31.get()==1:
        textbufalo.config(state=NORMAL)
        textbufalo.delete(0, END)
        textbufalo.focus()
    else:
        textbufalo.config(state=DISABLED)
        a_bbq.set('0')

def bufalo_a12():
    if var41.get()==1:
        textbufalo12.config(state=NORMAL)
        textbufalo12.delete(0, END)
        textbufalo12.focus()
    else:
        textbufalo12.config(state=DISABLED)
        a_bbq12.set('0')

def bufalo_a18():
    if var42.get()==1:
        textbufalo18.config(state=NORMAL)
        textbufalo18.delete(0, END)
        textbufalo18.focus()
    else:
        textbufalo18.config(state=DISABLED)
        a_bbq18.set('0')

def mango_a():
    if var32.get()==1:
        textmango.config(state=NORMAL)
        textmango.delete(0, END)
        textmango.focus()
    else:
        textmango.config(state=DISABLED)
        a_mango.set('0')

def mango_a12():
    if var43.get()==1:
        textmango12.config(state=NORMAL)
        textmango12.delete(0, END)
        textmango12.focus()
    else:
        textmango12.config(state=DISABLED)
        a_mango12.set('0')

def mango_a18():
    if var44.get()==1:
        textmango18.config(state=NORMAL)
        textmango18.delete(0, END)
        textmango18.focus()
    else:
        textmango18.config(state=DISABLED)
        a_mango18.set('0')

def piña_a():
    if var33.get()==1:
        textpiña.config(state=NORMAL)
        textpiña.delete(0, END)
        textpiña.focus()
    else:
        textpiña.config(state=DISABLED)
        a_piña.set('0')

def piña_a12():
    if var45.get()==1:
        textpiña12.config(state=NORMAL)
        textpiña12.delete(0, END)
        textpiña12.focus()
    else:
        textpiña12.config(state=DISABLED)
        a_piña12.set('0')

def piña_a18():
    if var46.get()==1:
        textpiña18.config(state=NORMAL)
        textpiña18.delete(0, END)
        textpiña18.focus()
    else:
        textmango18.config(state=DISABLED)
        a_mango18.set('0')

def habanero_a():
    if var34.get()==1:
        texthabanero.config(state=NORMAL)
        texthabanero.delete(0, END)
        texthabanero.focus()
    else:
        texthabanero.config(state=DISABLED)
        a_habanero.set('0')

def habanero_a12():
    if var47.get()==1:
        texthabanero12.config(state=NORMAL)
        texthabanero12.delete(0, END)
        texthabanero12.focus()
    else:
        texthabanero12.config(state=DISABLED)
        a_habanero12.set('0')

def habanero_a18():
    if var48.get()==1:
        texthabanero18.config(state=NORMAL)
        texthabanero18.delete(0, END)
        texthabanero18.focus()
    else:
        texthabanero18.config(state=DISABLED)
        a_habanero18.set('0')
def tamarindo_a():
    if var35.get()==1:
        texttamarindo.config(state=NORMAL)
        texttamarindo.delete(0, END)
        texttamarindo.focus()
    else:
        texttamarindo.config(state=DISABLED)
        a_tamarindo.set('0')

def tamarindo_a12():
    if var49.get()==1:
        texttamarindo12.config(state=NORMAL)
        texttamarindo12.delete(0, END)
        texttamarindo12.focus()
    else:
        texttamarindo12.config(state=DISABLED)
        a_tamarindo12.set('0')

def tamarindo_a18():
    if var50.get()==1:
        texttamarindo18.config(state=NORMAL)
        texttamarindo18.delete(0, END)
        texttamarindo18.focus()
    else:
        texttamarindo18.config(state=DISABLED)
        a_tamarindo18.set('0')
def quesopar_a():
    if var36.get()==1:
        textquesopar.config(state=NORMAL)
        textquesopar.delete(0, END)
        textquesopar.focus()
    else:
        textquesopar.config(state=DISABLED)
        a_quesopar.set('0')

def quesopar_a12():
    if var51.get()==1:
        textquesopar12.config(state=NORMAL)
        textquesopar12.delete(0, END)
        textquesopar12.focus()
    else:
        textquesopar12.config(state=DISABLED)
        a_quesopar12.set('0')

def quesopar_a18():
    if var52.get()==1:
        textquesopar18.config(state=NORMAL)
        textquesopar18.delete(0, END)
        textquesopar18.focus()
    else:
        textquesopar18.config(state=DISABLED)
        a_quesopar18.set('0')
def valentina_a():
    if var37.get()==1:
        textvalentina.config(state=NORMAL)
        textvalentina.delete(0, END)
        textvalentina.focus()
    else:
        textvalentina.config(state=DISABLED)
        a_valentina.set('0')

def valentina_a12():
    if var53.get()==1:
        textvalentina12.config(state=NORMAL)
        textvalentina12.delete(0, END)
        textvalentina12.focus()
    else:
        textvalentina12.config(state=DISABLED)
        a_valentina12.set('0')

def valentina_a18():
    if var54.get()==1:
        textvalentina18.config(state=NORMAL)
        textvalentina18.delete(0, END)
        textvalentina18.focus()
    else:
        textvalentina18.config(state=DISABLED)
        a_valentina18.set('0')

def ranch_a():
    if var38.get()==1:
        textranch.config(state=NORMAL)
        textranch.delete(0, END)
        textranch.focus()
    else:
        textranch.config(state=DISABLED)
        a_ranch.set('0')

def ranch_a12():
    if var55.get()==1:
        textranch12.config(state=NORMAL)
        textranch12.delete(0, END)
        textranch12.focus()
    else:
        textranch12.config(state=DISABLED)
        a_ranch12.set('0')

def ranch_a18():
    if var56.get()==1:
        textranch18.config(state=NORMAL)
        textranch18.delete(0, END)
        textranch18.focus()
    else:
        textranch18.config(state=DISABLED)
        a_ranch18.set('0')
#Funciones Bebidas
def pepsi_b():
    if var18.get()==1:
        textpepsi.config(state=NORMAL)
        textpepsi.delete(0, END)
        textpepsi.focus()
    else:
        textpepsi.config(state=DISABLED)
        b_pepsi.set('0')

def manzana_b():
    if var19.get()==1:
        textmanzana.config(state=NORMAL)
        textmanzana.delete(0, END)
        textmanzana.focus()
    else:
        textmanzana.config(state=DISABLED)
        b_manzana.set('0')

def mirinda_b():
    if var20.get()==1:
        textmirinda.config(state=NORMAL)
        textmirinda.delete(0, END)
        textmirinda.focus()
    else:
        textmirinda.config(state=DISABLED)
        b_mirinda.set('0')

def seven_b():
    if var21.get()==1:
        textseven.config(state=NORMAL)
        textseven.delete(0, END)
        textseven.focus()
    else:
        textseven.config(state=DISABLED)
        b_seven.set('0')

def sangria_b():
    if var22.get()==1:
        textsangria.config(state=NORMAL)
        textsangria.delete(0, END)
        textsangria.focus()
    else:
        textsangria.config(state=DISABLED)
        b_sangria.set('0')

def delaware_b():
    if var23.get()==1:
        textdelaware.config(state=NORMAL)
        textdelaware.delete(0, END)
        textdelaware.focus()
    else:
        textdelaware.config(state=DISABLED)
        b_delaware.set('0')

def jumex_b():
    if var24.get()==1:
        textjumex.config(state=NORMAL)
        textjumex.delete(0, END)
        textjumex.focus()
    else:
        textjumex.config(state=DISABLED)
        b_jumex.set('0')

def refresco_b():
    if var25.get()==1:
        textrefresco.config(state=NORMAL)
        textrefresco.delete(0, END)
        textrefresco.focus()
    else:
        textrefresco.config(state=DISABLED)
        b_refresco.set('0')

def bonafont_b():
    if var26.get()==1:
        textrbonafont.config(state=NORMAL)
        textrbonafont.delete(0, END)
        textrbonafont.focus()
    else:
        textrbonafont.config(state=DISABLED)
        b_bonafont.set('0')

def cocacola_b():
    if var27.get()==1:
        textcocacola.config(state=NORMAL)
        textcocacola.delete(0, END)
        textcocacola.focus()
    else:
        textcocacola.config(state=DISABLED)
        b_cocacola.set('0')

def refresco2l_b():
    if var28.get()==1:
        textrefresco2l.config(state=NORMAL)
        textrefresco2l.delete(0, END)
        textrefresco2l.focus()
    else:
        textrefresco2l.config(state=DISABLED)
        b_refresco2.set('0')

def bonafontlevite_b():
    if var29.get()==1:
        textrbonafontlevite.config(state=NORMAL)
        textrbonafontlevite.delete(0, END)
        textrbonafontlevite.focus()
    else:
        textrbonafontlevite.config(state=DISABLED)
        b_bonafontlevite.set('0')


#Funciones Extras
def pulpos_x():
    if var11.get()==1:
        textpulpos.config(state=NORMAL)
        textpulpos.delete(0, END)
        textpulpos.focus()
    else:
        textpulpos.config(state=DISABLED)
        x_pulpos.set('0')

def francesa_x():
    if var12.get()==1:
        textpapafran.config(state=NORMAL)
        textpapafran.delete(0, END)
        textpapafran.focus()
    else:
        textpapafran.config(state=DISABLED)
        x_papafran.set('0')

def salchipa_x():
    if var13.get()==1:
        textsalchipapa.config(state=NORMAL)
        textsalchipapa.delete(0, END)
        textsalchipapa.focus()
    else:
        textsalchipapa.config(state=DISABLED)
        x_salchipapa.set('0')

def salchimo_x():
    if var14.get()==1:
        textmomia.config(state=NORMAL)
        textmomia.delete(0, END)
        textmomia.focus()
    else:
        textmomia.config(state=DISABLED)
        x_momia.set('0')

def cebolla_x():
    if var15.get()==1:
        textcebolla.config(state=NORMAL)
        textcebolla.delete(0, END)
        textcebolla.focus()
    else:
        textcebolla.config(state=DISABLED)
        x_cebolla.set('0')

def ddq_x():
    if var16.get()==1:
        textdedosdq.config(state=NORMAL)
        textdedosdq.delete(0, END)
        textdedosdq.focus()
    else:
        textdedosdq.config(state=DISABLED)
        x_dedosdq.set('0')

def nuggets_x():
    if var17.get()==1:
        textnuggets.config(state=NORMAL)
        textnuggets.delete(0, END)
        textnuggets.focus()
    else:
        textnuggets.config(state=DISABLED)
        x_nuggets.set('0')


ven_principal = Tk()
ven_principal.resizable(300,300)
ven_principal.title("THE BLACK ROOSTERS AND BURGERS")
ven_principal.config(bg="Yellow")

rec1 = Frame(ven_principal, bd=5, relief=RIDGE, bg="yellow")
rec1.pack(side=TOP)
tirec1 = Label(rec1, text="THE BLACK ROOSTER'S AND BURGER'S", font=('arial', 15, 'bold'), fg='yellow', bg='black')
tirec1.grid (row=0, column=0)

#Declaracion del menu 
#recuadro global y recuadros secundarios 
menu_gl = Frame(ven_principal, bd=5, relief=RIDGE, bg='black' )
menu_gl.pack(side=LEFT)
menu1 =  Frame(menu_gl, bd=5, relief=RIDGE, bg='black')
menu1.pack(side=BOTTOM)
menu2 = Frame(menu_gl, bd=5, relief=RIDGE, bg='black')
menu2.pack(side=BOTTOM)
menu3 = Frame(ven_principal, bd=5, relief=RIDGE, bg='black')
menu3.pack(side=LEFT)
menu4 = Frame(menu_gl, bd=5, relief=RIDGE, bg='black')
menu4.pack(side=BOTTOM)
hambur = LabelFrame(menu1, text="HAMBURGESAS", font=('arial', 12, 'bold'), bd=5, relief=RIDGE, bg='yellow', padx=28)
hambur.pack(side=LEFT)
alitas = LabelFrame(menu3, text="ALITAS", font=('arial', 12, 'bold'), bd=5, relief=RIDGE, bg='yellow', pady=174)
alitas.pack(side=LEFT)
hot_dogs = LabelFrame(menu1,text='HOT DOGS', font=('arial', 12, 'bold'), bd=5, relief=RIDGE, bg='yellow', padx=28)
hot_dogs.pack(side=LEFT)
bebidas = LabelFrame(menu2, text='BEBIDAS', font=('arial', 12, 'bold'), bd=5, relief=RIDGE, bg='yellow')
bebidas.pack(side=LEFT)
extras = LabelFrame(menu2, text='SNACKS EXTRAS', font=('arial', 12, 'bold'), bd=5, relief=RIDGE, bg='yellow', pady=65)
extras.pack(side=BOTTOM)

#Recuadro de calculadora
rec2 = Frame(ven_principal, bd=10, relief=RIDGE, bg='black', padx=8, pady=83)
rec2.pack(side=RIGHT)
calculadora = Frame(rec2, bd=1, relief=RIDGE, bg='black', padx=18, pady=2)
calculadora.pack()
reciep = Frame(rec2, bd=4, relief=RIDGE, bg='black')
reciep.pack()
botones = Frame(rec2, bd=4, relief=RIDGE, bg='black')
botones.pack()



#Declaracion de variables para la comida, empezando por Hamburgesas
var1 = IntVar()
var2 = IntVar()
var3 = IntVar()
var4 = IntVar()
var5 = IntVar()
var6 = IntVar()
var7 = IntVar()
var8 = IntVar()
var9 = IntVar()
var10 = IntVar()
var11 = IntVar()
var12= IntVar()
var13 = IntVar()
var14 = IntVar()
var15 = IntVar()
var16 = IntVar()
var17 = IntVar()
var18 = IntVar()
var19 = IntVar()
var20 = IntVar()
var21 = IntVar()
var22 = IntVar()
var23 = IntVar()
var24 = IntVar()
var25 = IntVar()
var26 = IntVar()
var27 = IntVar()
var28 = IntVar()
var29 = IntVar()
var30 = IntVar()
var31 = IntVar()
var32 = IntVar()
var33 = IntVar()
var34 = IntVar()
var35 = IntVar()
var36 = IntVar()
var37 = IntVar()
var38 = IntVar()
var39 = IntVar() #12
var40 = IntVar()
var41 = IntVar()#12
var42 = IntVar()
var43 = IntVar()#12
var44 = IntVar()
var45 = IntVar()#12
var46 = IntVar()
var47 = IntVar()#12
var48 = IntVar()
var49 = IntVar()#12
var50 = IntVar()
var51 = IntVar()#12
var52 = IntVar()
var53 = IntVar()#12
var54 = IntVar()
var55 = IntVar()#12
var56 = IntVar()
#Cajas de texto hamburguesas
e_clasica = StringVar()
e_hawaiana = StringVar()
e_italiana = StringVar()
e_tocino = StringVar()
e_mexicana = StringVar()
#Cajas de texto hot dogs
h_clasica = StringVar()
h_hawaiana = StringVar()
h_italiana = StringVar()
h_tocino = StringVar()
h_mexicana = StringVar()
#Cajas de extras
x_pulpos = StringVar()
x_papafran = StringVar()
x_salchipapa = StringVar()
x_momia = StringVar()
x_cebolla = StringVar()
x_dedosdq = StringVar()
x_nuggets = StringVar()
#Cajas bebidas
b_pepsi = StringVar()
b_manzana = StringVar()
b_mirinda = StringVar()
b_seven = StringVar()
b_sangria = StringVar()
b_delaware = StringVar()
b_jumex = StringVar()
b_refresco = StringVar()
b_bonafont = StringVar()
b_cocacola = StringVar()
b_refresco2 = StringVar()
b_bonafontlevite = StringVar()
#Cajas alitas
a_bbq =StringVar()
a_bbq12 = StringVar()
a_bbq18 = StringVar()
a_bufalo =StringVar()
a_bufalo12 =StringVar()
a_bufalo18 =StringVar()
a_mango =StringVar()
a_mango12 =StringVar()
a_mango18 =StringVar()
a_piña =StringVar()
a_piña12 =StringVar()
a_piña18 =StringVar()
a_habanero =StringVar()
a_habanero12 =StringVar()
a_habanero18 =StringVar()
a_tamarindo =StringVar()
a_tamarindo12 =StringVar()
a_tamarindo18 =StringVar()
a_quesopar =StringVar()
a_quesopar12 =StringVar()
a_quesopar18 =StringVar()
a_valentina =StringVar()
a_valentina12 =StringVar()
a_valentina18 =StringVar()
a_ranch =StringVar()
a_ranch12 =StringVar()
a_ranch18 =StringVar()
#Pagos de cada parte del menu
costo_bebida =  StringVar()
costo_comida = StringVar()
costo_total = StringVar()


#Habilitar cuadros de texto y dar variables
e_clasica.set('0')
e_hawaiana.set('0')
e_italiana.set('0')
e_tocino.set('0')
e_mexicana.set('0')
h_clasica.set('0')
h_hawaiana.set('0')
h_italiana.set('0')
h_tocino.set('0')
h_mexicana.set('0')
x_pulpos.set('0')
x_papafran.set('0')
x_salchipapa.set('0')
x_momia.set('0')
x_cebolla.set('0')
x_dedosdq.set('0')
x_nuggets.set('0')
b_pepsi.set('0')
b_manzana.set('0')
b_mirinda.set('0')
b_seven.set('0')
b_sangria.set('0')
b_delaware.set('0')
b_jumex.set('0')
b_refresco.set('0')
b_bonafont.set('0')
b_cocacola.set('0')
b_refresco2.set('0')
b_bonafontlevite.set('0')
a_bbq.set('0')
a_bbq12.set('0')
a_bbq18.set('0')
a_bufalo.set('0')
a_bufalo12.set('0')
a_bufalo18.set('0')
a_mango.set('0')
a_mango12.set('0')
a_mango18.set('0')
a_piña.set('0')
a_piña12.set('0')
a_piña18.set('0')
a_habanero.set('0')
a_habanero12.set('0')
a_habanero18.set('0')
a_tamarindo.set('0')
a_tamarindo12.set('0')
a_tamarindo18.set('0')
a_quesopar.set('0')
a_quesopar12.set('0')
a_quesopar18.set('0')
a_valentina.set('0')
a_valentina12.set('0')
a_valentina18.set('0')
a_ranch.set('0')
a_ranch12.set('0')
a_ranch18.set('0')



#Check button/Habilita la eleccion del menu
clasica = Checkbutton(hambur, text='CLASICA', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var1, command=clas)
clasica.grid(row=0, column=0, sticky=W)
hawaiana = Checkbutton(hambur, text='HAWAIANA', font=('arial', 10, 'bold'), bg='yellow', onvalue=1, offvalue=0, variable=var2, command=haw)
hawaiana.grid(row=1, column=0, sticky=W)
italiana = Checkbutton(hambur, text='ITALIANA', font=('arial', 10, 'bold'), bg='yellow', onvalue=1, offvalue=0, variable=var3, command=ita)
italiana.grid(row=2, column=0, sticky=W)
tocino = Checkbutton(hambur, text='BBQ TOCINO', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var4, command=tocino)
tocino.grid(row=3, column=0, sticky=W)
mexicana = Checkbutton(hambur, text='MEXICANA', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var5, command=mexi)
mexicana.grid(row=4, column=0, sticky=W)

#Cajas de texto para dar valores
textclasica = Entry(hambur, font=('arial', 10, 'bold'), bd=2, width=10, state=DISABLED, textvariable=e_clasica )
textclasica.grid(row=0, column=1)
texthawaiana = Entry(hambur, font=('arial', 10, 'bold'), bd=2, width=10, state=DISABLED, textvariable=e_hawaiana)
texthawaiana.grid(row=1, column=1)
textitaliana = Entry(hambur, font=('arial', 10, 'bold'), bd=2, width=10, state=DISABLED, textvariable=e_italiana)
textitaliana.grid(row=2, column=1)
texttocino = Entry(hambur, font=('arial', 10, 'bold'), bd=2, width=10, state=DISABLED, textvariable=e_tocino)
texttocino.grid(row=3, column=1)
textmexicana = Entry(hambur, font=('arial', 10, 'bold'), bd=2, width=10, state=DISABLED, textvariable=e_mexicana)
textmexicana.grid(row=4, column=1)

#HOT DOGS
hot_clasica = Checkbutton(hot_dogs, text='CLASICO', font=('arial', 10, 'bold'), bg='yellow', onvalue=1, offvalue=0, variable=var6, command=hot_clas)
hot_clasica.grid(row=0, column=0, sticky=W)
hot_hawaiana = Checkbutton(hot_dogs, text='HAWAIANO', font=('arial', 10, 'bold'), bg='yellow', onvalue=1, offvalue=0, variable=var7, command=hot_haw)
hot_hawaiana.grid(row=1, column=0, sticky=W)
hot_italiana = Checkbutton(hot_dogs, text='ITALIANO', font=('arial', 10, 'bold'), bg='yellow', onvalue=1, offvalue=0, variable=var8, command=hot_ita)
hot_italiana.grid(row=2, column=0, sticky=W)
hot_tocino = Checkbutton(hot_dogs, text='BBQ TOCINO', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var9, command=hot_tocino)
hot_tocino.grid(row=3, column=0, sticky=W)
hot_mexicana = Checkbutton(hot_dogs, text='MEXICANO', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var10, command=hot_mex)
hot_mexicana.grid(row=4, column=0, sticky=W)

#Cajas de texto para dar valores
textclasica_hot = Entry(hot_dogs, font=('arial', 10, 'bold'), bd=2, width=10, state=DISABLED, textvariable=h_clasica)
textclasica_hot.grid(row=0, column=1)
texthawaiana_hot = Entry(hot_dogs, font=('arial', 10, 'bold'), bd=2, width=10, state=DISABLED, textvariable=h_hawaiana)
texthawaiana_hot.grid(row=1, column=1)
textitaliana_hot = Entry(hot_dogs, font=('arial', 10, 'bold'), bd=2, width=10, state=DISABLED, textvariable=h_italiana)
textitaliana_hot.grid(row=2, column=1)
texttocino_hot = Entry(hot_dogs, font=('arial', 10, 'bold'), bd=2, width=10, state=DISABLED, textvariable=h_tocino)
texttocino_hot.grid(row=3, column=1)
textmexicana_hot = Entry(hot_dogs, font=('arial', 10, 'bold'), bd=2, width=10, state=DISABLED, textvariable=h_mexicana)
textmexicana_hot.grid(row=4, column=1)

#Check button/Habilita la eleccion del menu
pulpos = Checkbutton(extras, text='SALCHIPULPOS', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var11, command=pulpos_x)
pulpos.grid(row=0, column=0, sticky=W)
papafran = Checkbutton(extras, text='PAPAS A LA FRANCESA', font=('arial', 10, 'bold'), bg='yellow', onvalue=1, offvalue=0, variable=var12, command=francesa_x)
papafran.grid(row=1, column=0, sticky=W)
salchipapa = Checkbutton(extras, text='SALCHIPAPAS', font=('arial', 10, 'bold'), bg='yellow', onvalue=1, offvalue=0, variable=var13, command=salchipa_x)
salchipapa.grid(row=2, column=0, sticky=W)
salchimomia = Checkbutton(extras, text='SALCHIMOMIAS', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var14, command=salchimo_x)
salchimomia.grid(row=3, column=0, sticky=W)
arosdcebolla = Checkbutton(extras, text='AROS DE CEBOLLA', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var15, command=cebolla_x)
arosdcebolla.grid(row=4, column=0, sticky=W)
dedosdq = Checkbutton(extras, text='DEDOS DE QUESO', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var16, command=ddq_x)
dedosdq.grid(row=5, column=0, sticky=W)
nuggest= Checkbutton(extras, text='NUGGETS DE POLLO', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var17, command=nuggets_x)
nuggest.grid(row=6, column=0, sticky=W)

#Cajas de texto para dar valores
textpulpos = Entry(extras, font=('arial', 10, 'bold'), bd=2, width=10, state=DISABLED, textvariable=x_pulpos)
textpulpos.grid(row=0, column=1)
textpapafran = Entry(extras, font=('arial', 10, 'bold'), bd=2, width=10, state=DISABLED, textvariable=x_papafran)
textpapafran.grid(row=1, column=1)
textsalchipapa = Entry(extras, font=('arial', 10, 'bold'), bd=2, width=10, state=DISABLED, textvariable=x_salchipapa)
textsalchipapa.grid(row=2, column=1)
textmomia = Entry(extras, font=('arial', 10, 'bold'), bd=2, width=10, state=DISABLED, textvariable=x_momia)
textmomia.grid(row=3, column=1)
textcebolla = Entry(extras, font=('arial', 10, 'bold'), bd=2, width=10, state=DISABLED, textvariable=x_cebolla)
textcebolla.grid(row=4, column=1)
textdedosdq = Entry(extras, font=('arial', 10, 'bold'), bd=2, width=10, state=DISABLED, textvariable=x_dedosdq)
textdedosdq.grid(row=5, column=1)
textnuggets = Entry(extras, font=('arial', 10, 'bold'), bd=2, width=10, state=DISABLED, textvariable=x_nuggets)
textnuggets.grid(row=6, column=1)

#Check button/Habilita la eleccion del menu
pepsi= Checkbutton(bebidas, text='PEPSI', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var18, command=pepsi_b)
pepsi.grid(row=0, column=0, sticky=W)
manzana = Checkbutton(bebidas, text='MANZANITA', font=('arial', 10, 'bold'), bg='yellow', onvalue=1, offvalue=0, variable=var19, command=manzana_b)
manzana.grid(row=1, column=0, sticky=W)
mirinda = Checkbutton(bebidas, text='MIRINDA', font=('arial', 10, 'bold'), bg='yellow', onvalue=1, offvalue=0, variable=var20, command=mirinda_b)
mirinda.grid(row=2, column=0, sticky=W)
seven = Checkbutton(bebidas, text='7 UP', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var21, command=seven_b)
seven.grid(row=3, column=0, sticky=W)
sangria = Checkbutton(bebidas, text='SANGRIA', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var22, command=sangria_b)
sangria.grid(row=4, column=0, sticky=W)
delaware = Checkbutton(bebidas, text='DELAWARE PUNCH', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var23, command=delaware_b)
delaware.grid(row=5, column=0, sticky=W)
jumex= Checkbutton(bebidas, text='JUMEX', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var24, command=jumex_b)
jumex.grid(row=6, column=0, sticky=W)
refresco= Checkbutton(bebidas, text='REFRESCO', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var25, command=refresco_b)
refresco.grid(row=7, column=0, sticky=W)
bonafont= Checkbutton(bebidas, text='BONAFONT', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var26, command=bonafont_b)
bonafont.grid(row=8, column=0, sticky=W)
cocacola= Checkbutton(bebidas, text='COCA COLA', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var27, command=cocacola_b)
cocacola.grid(row=9, column=0, sticky=W)
refresco2l= Checkbutton(bebidas, text='REFRESCO 2L', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var28, command=refresco2l_b)
refresco2l.grid(row=10, column=0, sticky=W)
bonafotlevite= Checkbutton(bebidas, text='BONAFONT LEVITE', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var29, command=bonafontlevite_b)
bonafotlevite.grid(row=11, column=0, sticky=W)

#Cajas de texto para dar valores
textpepsi = Entry(bebidas, font=('arial', 10, 'bold'), bd=2, width=10, state=DISABLED, textvariable=b_pepsi)
textpepsi.grid(row=0, column=1)
textmanzana = Entry(bebidas, font=('arial', 10, 'bold'), bd=2, width=10, state=DISABLED, textvariable=b_manzana)
textmanzana.grid(row=1, column=1)
textmirinda = Entry(bebidas, font=('arial', 10, 'bold'), bd=2, width=10, state=DISABLED, textvariable=b_mirinda)
textmirinda.grid(row=2, column=1)
textseven = Entry(bebidas, font=('arial', 10, 'bold'), bd=2, width=10, state=DISABLED, textvariable=b_seven)
textseven.grid(row=3, column=1)
textsangria = Entry(bebidas, font=('arial', 10, 'bold'), bd=2, width=10, state=DISABLED, textvariable=b_sangria)
textsangria.grid(row=4, column=1)
textdelaware = Entry(bebidas, font=('arial', 10, 'bold'), bd=2, width=10, state=DISABLED, textvariable=b_delaware)
textdelaware.grid(row=5, column=1)
textjumex = Entry(bebidas, font=('arial', 10, 'bold'), bd=2, width=10, state=DISABLED, textvariable=b_jumex)
textjumex.grid(row=6, column=1)
textrefresco = Entry(bebidas, font=('arial', 10, 'bold'), bd=2, width=10, state=DISABLED, textvariable=b_refresco)
textrefresco.grid(row=7, column=1)
textrbonafont = Entry(bebidas, font=('arial', 10, 'bold'), bd=2, width=10, state=DISABLED, textvariable=b_bonafont)
textrbonafont.grid(row=8, column=1)
textcocacola = Entry(bebidas, font=('arial', 10, 'bold'), bd=2, width=10, state=DISABLED, textvariable=b_cocacola)
textcocacola.grid(row=9, column=1)
textrefresco2l = Entry(bebidas, font=('arial', 10, 'bold'), bd=2, width=10, state=DISABLED, textvariable=b_refresco2)
textrefresco2l.grid(row=10, column=1)
textrbonafontlevite = Entry(bebidas, font=('arial', 10, 'bold'), bd=2, width=10, state=DISABLED, textvariable=b_bonafontlevite)
textrbonafontlevite.grid(row=11, column=1)

#Check button/Habilita la eleccion del menu
bbq= Checkbutton(alitas, text='BBQ x6', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var30, command=bbq_a)
bbq.grid(row=0, column=0, sticky=W)
bbq12= Checkbutton(alitas, text='x12', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var39, command=bbq_a12)
bbq12.grid(row=0, column=2, sticky=W)
bbq18= Checkbutton(alitas, text='x18', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var40, command=bbq_a18)
bbq18.grid(row=0, column=4, sticky=W)
bufalo = Checkbutton(alitas, text='BUFALO x6', font=('arial', 10, 'bold'), bg='yellow', onvalue=1, offvalue=0, variable=var31, command=bufalo_a)
bufalo.grid(row=1, column=0, sticky=W)
bufalo12= Checkbutton(alitas, text='x12', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var41, command=bufalo_a12)
bufalo12.grid(row=1, column=2, sticky=W)
bufalo18= Checkbutton(alitas, text='x18', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var42, command=bufalo_a18)
bufalo18.grid(row=1, column=4, sticky=W)
mango = Checkbutton(alitas, text='MANGO HABANERO x6', font=('arial', 10, 'bold'), bg='yellow', onvalue=1, offvalue=0, variable=var32, command=mango_a)
mango.grid(row=2, column=0, sticky=W)
mango12= Checkbutton(alitas, text='x12', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var43, command=mango_a12)
mango12.grid(row=2, column=2, sticky=W)
mango18= Checkbutton(alitas, text='x18', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var44, command=mango_a18)
mango18.grid(row=2, column=4, sticky=W)
piña = Checkbutton(alitas, text='PIÑA HABANERO x6', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var33, command=piña_a)
piña.grid(row=3, column=0, sticky=W)
piña12= Checkbutton(alitas, text='x12', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var45, command=piña_a12)
piña12.grid(row=3, column=2, sticky=W)
piña18= Checkbutton(alitas, text='x18', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var46, command=piña_a18)
piña18.grid(row=3, column=4, sticky=W)
habanero = Checkbutton(alitas, text='HABANERO x6', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var34, command=habanero_a)
habanero.grid(row=4, column=0, sticky=W)
habanero12= Checkbutton(alitas, text='x12', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var47, command=habanero_a12)
habanero12.grid(row=4, column=2, sticky=W)
habanero18= Checkbutton(alitas, text='x18', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var48, command=habanero_a18)
habanero18.grid(row=4, column=4, sticky=W)
tamarindo = Checkbutton(alitas, text='TAMARINDO CHIPOTLE x6', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var35, command=tamarindo_a)
tamarindo.grid(row=5, column=0, sticky=W)
tamarindo12= Checkbutton(alitas, text='x12', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var49, command=tamarindo_a12)
tamarindo12.grid(row=5, column=2, sticky=W)
tamarindo18= Checkbutton(alitas, text='x18', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var50, command=tamarindo_a18)
tamarindo18.grid(row=5, column=4, sticky=W)
quesopar= Checkbutton(alitas, text='QUESO PARMESANO x6', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var36, command=quesopar_a)
quesopar.grid(row=6, column=0, sticky=W)
quesopar12= Checkbutton(alitas, text='x12', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var51, command=quesopar_a12)
quesopar12.grid(row=6, column=2, sticky=W)
quesopar18= Checkbutton(alitas, text='x18', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var52, command=quesopar_a18)
quesopar18.grid(row=6, column=4, sticky=W)
valentina= Checkbutton(alitas, text='VALENTINA x6', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var37, command=valentina_a)
valentina.grid(row=7, column=0, sticky=W)
valentina12= Checkbutton(alitas, text='x12', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var53, command=valentina_a12)
valentina12.grid(row=7, column=2, sticky=W)
valentina18= Checkbutton(alitas, text='x18', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var54, command=valentina_a18)
valentina18.grid(row=7, column=4, sticky=W)
ranch= Checkbutton(alitas, text='RANCH x6', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var38, command=ranch_a)
ranch.grid(row=8, column=0, sticky=W)
ranch12= Checkbutton(alitas, text='x12', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var55, command=ranch_a12)
ranch12.grid(row=8, column=2, sticky=W)
ranch18= Checkbutton(alitas, text='x18', font=('arial', 10, 'bold'), bg='yellow',onvalue=1, offvalue=0, variable=var56, command=ranch_a18)
ranch18.grid(row=8, column=4, sticky=W)
#Cajas de texto para dar valores
textbbq = Entry(alitas, font=('arial', 10, 'bold'), bd=2, width=5, state=DISABLED, textvariable=a_bbq)
textbbq.grid(row=0, column=1)
textbbq12 = Entry(alitas, font=('arial', 10, 'bold'), bd=2, width=5, state=DISABLED, textvariable=a_bbq12)
textbbq12.grid(row=0, column=3)
textbbq18 = Entry(alitas, font=('arial', 10, 'bold'), bd=2, width=5, state=DISABLED, textvariable=a_bbq18)
textbbq18.grid(row=0, column=5)
textbufalo = Entry(alitas, font=('arial', 10, 'bold'), bd=2, width=5, state=DISABLED, textvariable=a_bufalo)
textbufalo.grid(row=1, column=1)
textbufalo12 = Entry(alitas, font=('arial', 10, 'bold'), bd=2, width=5, state=DISABLED, textvariable=a_bufalo12)
textbufalo12.grid(row=1, column=3)
textbufalo18 = Entry(alitas, font=('arial', 10, 'bold'), bd=2, width=5, state=DISABLED, textvariable=a_bufalo18)
textbufalo18.grid(row=1, column=5)
textmango = Entry(alitas, font=('arial', 10, 'bold'), bd=2, width=5, state=DISABLED, textvariable=a_mango)
textmango.grid(row=2, column=1)
textmango12 = Entry(alitas, font=('arial', 10, 'bold'), bd=2, width=5, state=DISABLED, textvariable=a_mango12)
textmango12.grid(row=2, column=3)
textmango18 = Entry(alitas, font=('arial', 10, 'bold'), bd=2, width=5, state=DISABLED, textvariable=a_mango18)
textmango18.grid(row=2, column=5)
textpiña = Entry(alitas, font=('arial', 10, 'bold'), bd=2, width=5, state=DISABLED, textvariable=a_piña)
textpiña.grid(row=3, column=1)
textpiña12 = Entry(alitas, font=('arial', 10, 'bold'), bd=2, width=5, state=DISABLED, textvariable=a_piña12)
textpiña12.grid(row=3, column=3)
textpiña18 = Entry(alitas, font=('arial', 10, 'bold'), bd=2, width=5, state=DISABLED, textvariable=a_piña18)
textpiña18.grid(row=3, column=5)
texthabanero = Entry(alitas, font=('arial', 10, 'bold'), bd=2, width=5, state=DISABLED, textvariable=a_habanero)
texthabanero.grid(row=4, column=1)
texthabanero12 = Entry(alitas, font=('arial', 10, 'bold'), bd=2, width=5, state=DISABLED, textvariable=a_habanero12)
texthabanero12.grid(row=4, column=3)
texthabanero18 = Entry(alitas, font=('arial', 10, 'bold'), bd=2, width=5, state=DISABLED, textvariable=a_habanero18)
texthabanero18.grid(row=4, column=5)
texttamarindo = Entry(alitas, font=('arial', 10, 'bold'), bd=2, width=5, state=DISABLED, textvariable=a_tamarindo)
texttamarindo.grid(row=5, column=1)
texttamarindo12 = Entry(alitas, font=('arial', 10, 'bold'), bd=2, width=5, state=DISABLED, textvariable=a_tamarindo12)
texttamarindo12.grid(row=5, column=3)
texttamarindo18 = Entry(alitas, font=('arial', 10, 'bold'), bd=2, width=5, state=DISABLED, textvariable=a_tamarindo18)
texttamarindo18.grid(row=5, column=5)
textquesopar = Entry(alitas, font=('arial', 10, 'bold'), bd=2, width=5, state=DISABLED, textvariable=a_quesopar)
textquesopar.grid(row=6, column=1)
textquesopar12 = Entry(alitas, font=('arial', 10, 'bold'), bd=2, width=5, state=DISABLED, textvariable=a_quesopar12)
textquesopar12.grid(row=6, column=3)
textquesopar18 = Entry(alitas, font=('arial', 10, 'bold'), bd=2, width=5, state=DISABLED, textvariable=a_quesopar18)
textquesopar18.grid(row=6, column=5)
textvalentina = Entry(alitas, font=('arial', 10, 'bold'), bd=2, width=5, state=DISABLED, textvariable=a_valentina)
textvalentina.grid(row=7, column=1)
textvalentina12 = Entry(alitas, font=('arial', 10, 'bold'), bd=2, width=5, state=DISABLED, textvariable=a_valentina12)
textvalentina12.grid(row=7, column=3)
textvalentina18 = Entry(alitas, font=('arial', 10, 'bold'), bd=2, width=5, state=DISABLED, textvariable=a_valentina18)
textvalentina18.grid(row=7, column=5)
textranch = Entry(alitas, font=('arial', 10, 'bold'), bd=2, width=5, state=DISABLED, textvariable=a_ranch)
textranch.grid(row=8, column=1)
textranch12 = Entry(alitas, font=('arial', 10, 'bold'), bd=2, width=5, state=DISABLED, textvariable=a_ranch12)
textranch12.grid(row=8, column=3)
textranch18 = Entry(alitas, font=('arial', 10, 'bold'), bd=2, width=5, state=DISABLED, textvariable=a_ranch18)
textranch18.grid(row=8, column=5)

#Totales de pago y pago total
costobebida = Label(menu4, text='TOTAL DE BEBIDAS', font=('arial', 10, 'bold'), bd=5, relief=RIDGE, bg='yellow', padx=110)
costobebida.grid(row=0, column=0)
textcostbebida = Entry(menu4, font=('arial', 10, 'bold'), bd=5, state='readonly', textvariable=costo_bebida)
textcostbebida.grid(row=0, column=1)
costocomida = Label(menu4, text='TOTAL DE COMIDA', font=('arial', 10, 'bold'), bd=5, relief=RIDGE, bg='yellow',  padx=113)
costocomida.grid(row=1, column=0)
textcomida = Entry(menu4, font=('arial', 10, 'bold'), bd=5, state='readonly', textvariable=costo_comida)
textcomida.grid(row=1, column=1)
pagototal = Label(menu4, text='PAGO TOTAL', font=('arial', 10, 'bold'), bd=5, relief=RIDGE, bg='yellow',  padx=130)
pagototal.grid(row=2, column=0)
pago_total = Entry(menu4, font=('arial', 10, 'bold'), bd=5, state='readonly', textvariable=costo_total )
pago_total.grid(row=2, column=1)

#Botones de la calculadora
btn_total = Button(botones, text='TOTAL', font=('arial', 10, 'bold'), fg='yellow', bg='black', bd=3, command=total_costo)
btn_total.grid(row=0, column=0)
btn_capturar = Button(botones, text='CAPTURAR', font=('arial', 10, 'bold'), fg='yellow', bg='black', bd=3, command=capturarorden)
btn_capturar.grid(row=0, column=1)
btn_save = Button(botones, text='GUARDAR', font=('arial', 10, 'bold'), fg='yellow', bg='black', bd=3, command=guardar_2)
btn_save.grid(row=0, column=2)
btn_send = Button(botones, text='ENVIAR', font=('arial', 10, 'bold'), fg='yellow', bg='black', bd=3)
btn_send.grid(row=0, column=3)
btn_reset = Button(botones, text='REINICIAR', font=('arial', 10, 'bold'), fg='yellow', bg='black', bd=3, command=reiniciar_2)
btn_reset.grid(row=0, column=4)

#Area de texto para receipt
textcaptura = Text(reciep, font=('arial', 10, 'bold'),bd=3, width=48, height=15)
textcaptura.grid(row=0, column=0)

#Calculadora
operador =''
def btnclick(numeros):
    global operador
    operador = operador + numeros
    calculadorafield.delete(0, END)
    calculadorafield.insert(END, operador)

def clear():
    global operador
    operador = ''
    calculadorafield.delete(0, END)

def answer():
    global operador
    result = str(eval(operador))
    calculadorafield.delete(0, END)
    calculadorafield.insert(0,result)
    operador = ''

calculadorafield = Entry(calculadora, font=('arial', 10, 'bold'), width=32, bd=2)
calculadorafield.grid(row=0, column=0, columnspan=4)

btn7 = Button(calculadora, text='7', font=('arial', 10, 'bold'),fg='yellow', bg='black', bd=2, width=6, command=lambda:btnclick('7'))
btn7.grid(row=1, column=0)
btn8 = Button(calculadora, text='8', font=('arial', 10, 'bold'),fg='yellow', bg='black', bd=2, width=6, command=lambda:btnclick('8'))
btn8.grid(row=1, column=1)
btn9 = Button(calculadora, text='9', font=('arial', 10, 'bold'),fg='yellow', bg='black', bd=2, width=6, command=lambda:btnclick('9'))
btn9.grid(row=1, column=2)
btnsuma = Button(calculadora, text='+', font=('arial', 10, 'bold'),fg='yellow', bg='black', bd=2, width=6, command=lambda:btnclick('+'))
btnsuma.grid(row=1, column=3)
btn4 = Button(calculadora, text='4', font=('arial', 10, 'bold'),fg='yellow', bg='black', bd=2, width=6, command=lambda:btnclick('4'))
btn4.grid(row=2, column=0)
btn5 = Button(calculadora, text='5', font=('arial', 10, 'bold'),fg='yellow', bg='black', bd=2, width=6, command=lambda:btnclick('5'))
btn5.grid(row=2, column=1)
btn6 = Button(calculadora, text='6', font=('arial', 10, 'bold'),fg='yellow', bg='black', bd=2, width=6, command=lambda:btnclick('6'))
btn6.grid(row=2, column=2)
btnresta = Button(calculadora, text='-', font=('arial', 10, 'bold'),fg='yellow', bg='black', bd=2, width=6, command=lambda:btnclick('-'))
btnresta.grid(row=2, column=3)
btn1 = Button(calculadora, text='1', font=('arial', 10, 'bold'),fg='yellow', bg='black', bd=2, width=6, command=lambda:btnclick('1'))
btn1.grid(row=3, column=0)
btn2 = Button(calculadora, text='2', font=('arial', 10, 'bold'),fg='yellow', bg='black', bd=2, width=6, command=lambda:btnclick('2'))
btn2.grid(row=3, column=1)
btn3 = Button(calculadora, text='3', font=('arial', 10, 'bold'),fg='yellow', bg='black', bd=2, width=6, command=lambda:btnclick('3'))
btn3.grid(row=3, column=2)
btnimul = Button(calculadora, text='*', font=('arial', 10, 'bold'),fg='yellow', bg='black', bd=2, width=6, command=lambda:btnclick('*'))
btnimul.grid(row=3, column=3)
btnans = Button(calculadora, text='=', font=('arial', 10, 'bold'),fg='yellow', bg='black', bd=2, width=6, command=answer)
btnans.grid(row=4, column=0)
btnborrar = Button(calculadora, text='BORRAR', font=('arial', 10, 'bold'),fg='yellow', bg='black', bd=2, width=6, command=clear)
btnborrar.grid(row=4, column=1)
btn0 = Button(calculadora, text='0', font=('arial', 10, 'bold'),fg='yellow', bg='black', bd=2, width=6, command=lambda:btnclick('0'))
btn0.grid(row=4, column=2)
btndiv = Button(calculadora, text='/', font=('arial', 10, 'bold'),fg='yellow', bg='black', bd=2, width=6, command=lambda:btnclick('/'))
btndiv.grid(row=4, column=3)

ven_principal.mainloop()